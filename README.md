# LoGraThor

![Build status](https://git.astron.nl/lofar2.0/lograthor/badges/main/pipeline.svg)
![Test coverage](https://git.astron.nl/lofar2.0/lograthor/badges/main/coverage.svg)

LOFAR 2.0 Multi Project Integrat(h)or (LoGraThor).

LoGraThor is an all encompassing Python integration test bench library. It is able to
start, configure and  monitor processes prior to running tests as well as reasoning
about systems, components and their subcomponents. The  entire list of principles
LoGraThor helps facilitate is listed below.

## Key Facilitating Principles

1. Composition, the ability to reason about parts of a system as part of another
   (bigger, wider, taller) system.
2. Orthogonality, tests should not influence each other and the order in which they are
   executed should not matter for the results.
3. Deterministic, the result for each test should be the same each time it is executed
   (given the same `environment` and configuration).
4. Fairness, each test, should be provided a system that is in the same
   state as for every other test (from the same `environment`).
5. Resilience, all tested services should still be in a working state after the test has
   finished.

## Installation

```shell
pip install .
```

## Usage

```shell
lograthor -- test-directory
```

## Contributing

To contribute, please create a feature branch and a "Draft" merge request.
Upon completion, the merge request should be marked as ready and a reviewer
should be assigned.

Verify your changes locally and be sure to add tests. Verifying local
changes is done through `tox`.

```shell
pip install tox
```

With tox the same jobs as run on the CI/CD pipeline can be ran. These
include unit tests and linting.

```shell
tox
```

To automatically apply most suggested linting changes execute:

```shell
tox -e format
```

## License
This project is licensed under the Apache License Version 2.0
