# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import sys
import time

import pytest

from lograthor.arguments.arguments import parse_arguments
from lograthor.config import config
from lograthor.config.options import ConfigOptions
from lograthor.arguments import arguments
from lograthor.logging import logging
from lograthor.module import git
from lograthor.shutdown import shutdown

logger = logging.get_logger()


def main():
    shutdown.register_handler()
    config_options = ConfigOptions()
    args = arguments.split_arguments(sys.argv[1:])

    logging_level = None
    if len(args) > 0:
        config_options = parse_arguments(args[0])
        logging_level = config_options.logging_level
        logging.configure(level=logging_level)
        logger.debug("LoGraThor arguments: %s", args[0])
    else:
        logging.configure(level=logging_level)

    git.checkout_env_modules_restore_on_exit(config_options.module_directory)

    time.sleep(5)

    pytest_args = config.get_pytest_default_arguments()
    if len(args) >= 2:
        pytest_args += args[1]

    logger.debug("PyTest arguments: %s", pytest_args)

    sys.exit(pytest.main(pytest_args))  # plugins=[MyPlugin()]))
