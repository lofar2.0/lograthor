# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0


def greeter():
    """Prints a nice message"""
    print("Hello World!")
