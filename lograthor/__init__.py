""" LoGraThor """

try:
    from importlib import metadata
except ImportError:  # for Python<3.8
    import importlib_metadata as metadata

package_name = "lograthor"
__version__ = metadata.version("lograthor")
