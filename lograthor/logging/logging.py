# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

from lograthor import package_name
from lograthor.config.defaults import DEFAULT_LOG_LEVEL


def configure(logger=None, level=None):
    """Configure logger to show output"""

    if not level:
        level = DEFAULT_LOG_LEVEL

    logging.basicConfig(
        format="%(asctime)s - %(levelname)s [%(name)s][%(filename)s:%(funcName)s:"
        "%(lineno)d] - %(message)s",
        level=level,
    )

    if not logger:
        logger = logging.getLogger(package_name)

    logger.setLevel(level)

    logging.getLogger("git").setLevel(logging.ERROR)


def get_logger():
    """Get logger for this library"""
    return logging.getLogger(package_name)
