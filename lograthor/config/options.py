# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from dataclasses import dataclass

from lograthor.config.defaults import DEFAULT_LOG_LEVEL
from lograthor.config.defaults import DEFAULT_MODULE_DIRECTORY


@dataclass
class ConfigOptions:
    """All configuration options recognized by LoGraThor"""

    logging_level: int = DEFAULT_LOG_LEVEL
    module_directory: str = DEFAULT_MODULE_DIRECTORY
