# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from typing import List

from lograthor.config.defaults import DEFAULT_PYTEST_ARGUMENTS


def get_pytest_default_arguments() -> List[str]:
    return DEFAULT_PYTEST_ARGUMENTS
