# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

DEFAULT_LOG_LEVEL = logging.INFO
DEFAULT_MODULE_DIRECTORY = "modules"
DEFAULT_PYTEST_ARGUMENTS = ["-v", "-rpP", "--show-capture=log"]
