# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import os
from functools import partial
from typing import Dict

from git import Commit
from git import Repo

from lograthor.logging.logging import get_logger
from lograthor.shutdown import shutdown

logger = get_logger()


def update_submodules(working_directory: str):
    """Update all submodules for the repository in the directory"""
    repo = Repo(working_directory)

    if repo.bare:
        raise FileNotFoundError(
            f"A git repository could not be found in {working_directory}"
        )

    if len(repo.submodules) == 0:
        logger.warning("Not submodules could be found in repository: %s", repo)

    logger.warning("Updating modules: %s", [x.name for x in repo.submodules])
    repo.git.submodule("update", "--init")


def get_submodules_heads(module_directory: str) -> Dict[Repo, Commit]:
    """Return a dict of git repositories and their current heads"""
    results = {}
    for module in os.listdir(module_directory):
        repo = Repo(module_directory + "/" + module)
        if repo.bare:
            raise FileNotFoundError(
                f"A git repository for {module_directory}/{module} could not be found"
            )
        results[repo] = repo.head.commit
    return results


def checkout_env_submodule_version(module_directory: str):
    """Checkout submodule version based on environment variables"""

    logger.debug("Checking for modules in: %s", module_directory)
    for module in os.listdir(module_directory):
        mod_ver = f"{module.upper()}_VERSION"
        if mod_ver not in os.environ:
            continue

        repo = Repo(module_directory + "/" + module)
        if repo.bare:
            raise FileNotFoundError(
                f"A git repository for {module_directory}/{module} could not be found"
            )

        logger.info('Checkout "%s" for module: %s', os.environ[mod_ver], module)
        repo.git.fetch("origin", os.environ[mod_ver])
        repo.git.checkout(os.environ[mod_ver])
        repo.git.pull("origin", os.environ[mod_ver])


def checkout_env_modules_restore_on_exit(module_directory: str):
    """Checkout git modules commit using environment variables, restore head on exit"""
    modules_state = get_submodules_heads(module_directory)
    update_submodules(os.getcwd())

    def restore_module(repo: Repo, commit: Commit):
        repo.git.checkout(commit)

    for module, state in modules_state.items():
        shutdown.register(partial(restore_module, module, state))

    checkout_env_submodule_version(module_directory)
