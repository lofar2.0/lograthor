# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import argparse
from typing import List

from lograthor.config.defaults import DEFAULT_LOG_LEVEL
from lograthor.config.options import ConfigOptions


def split_arguments(arguments: List[str]) -> List[List[str]]:
    """Split the arguments with every occurrence of '--'"""

    split_args = [[]]
    index = 0
    for arg in arguments:
        if arg == "--":
            index += 1
            split_args.append([])
            continue

        split_args[index].append(arg)

    return split_args


def parse_arguments(arguments: List[str]) -> ConfigOptions:
    """Parse command line arguments for LoGraThor"""

    config_options = ConfigOptions()

    parser = argparse.ArgumentParser(
        description="LoGraThor multi-project integration test toolkit",
        usage="%(prog)s [options] -- [pytest options]",
    )
    parser.add_argument(
        "--log_level",
        "-L",
        type=int,
        default=DEFAULT_LOG_LEVEL,
        help="Minimum logging level for messages to be logged",
    )

    args = parser.parse_args(arguments)

    config_options.logging_level = args.log_level
    return config_options
