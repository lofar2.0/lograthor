# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import atexit
import signal
from typing import Callable

from lograthor.logging.logging import get_logger

logger = get_logger()

_exit_methods = []


def register(func: Callable[[], None]):
    """Register a function to be called prior to shutting down"""
    atexit.register(func)
    _exit_methods.append(func)


def unregister(func: Callable[[], None]):
    """Unregister a previously registered function from executing upon shutdown"""
    if func in _exit_methods:
        _exit_methods.remove(func)


def handler(*args):
    """Handle currently registered shutdown functions"""
    for func in _exit_methods:
        try:
            logger.info("Executing: %s", func)
            func()
            atexit.unregister(func)
        except BaseException:
            continue

    _exit_methods.clear()


def register_handler():
    """Register handler for various signals"""
    signal.signal(signal.SIGTERM, handler)
    signal.signal(signal.SIGINT, handler)
