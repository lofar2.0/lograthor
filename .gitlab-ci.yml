default:
  image: $CI_REGISTRY_IMAGE/ci-build-runner:$CI_COMMIT_REF_SLUG
  before_script:
    - python --version # For debugging
  cache:
    paths:
      - .cache/pip
      # Do not cache .tox, to recreate virtualenvs for every step

stages:
  - prepare
  - lint
  - test
  - package
  - integration
  - publish # publish instead of deploy

# Caching of dependencies to speed up builds
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"


# Prepare image to run ci on
trigger_prepare:
  stage: prepare
  trigger:
    strategy: depend
    include: .prepare.gitlab-ci.yml

run_black:
  stage: lint
  script:
    - tox -e black

run_flake8:
  stage: lint
  script:
    - tox -e pep8

run_pylint:
  stage: lint
  script:
    - tox -e pylint
  allow_failure: true

# Basic setup for all Python versions for which we don't have a base image
.run_unit_test_version_base:
  before_script:
    - python --version # For debugging
    - python -m pip install --upgrade pip
    - pip install --upgrade tox twine

# Run all unit tests for Python versions except the base image
run_unit_tests:
  extends: .run_unit_test_version_base
  stage: test
  image: python:3.${PY_VERSION}
  script:
    - tox -e py3${PY_VERSION}
  parallel:
    matrix: # use the matrix for testing
      - PY_VERSION: [7, 8, 9, 10]

# Run code coverage on the base image thus also performing unit tests
run_unit_tests_coverage:
  stage: test
  script:
   - tox -e coverage
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - htmlcov/*

package_files:
  stage: package
  artifacts:
    expire_in: 1w
    paths:
      - dist/*
  script:
    - tox -e build

package_docs:
  stage: package
  allow_failure: true
  artifacts:
    expire_in: 1w
    paths:
      - docs/* # update path to match the dest dir for documentation
  script:
    - echo "build and collect docs"
    - exit 1

run_integration_tests:
  stage: integration
  allow_failure: true
  needs:
    - package_files
  script:
    - echo "make sure to move out of source dir"
    - echo "install package from filesystem (or use the artefact)"
    - echo "run against foreign systems (e.g. databases, cwl etc.)"
    - exit 1

publish_on_gitlab:
  stage: publish
  environment: gitlab
  needs:
    - package_files
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
  script:
    - echo "run twine for gitlab"
    - |
      TWINE_PASSWORD=${CI_JOB_TOKEN} \
      TWINE_USERNAME=gitlab-ci-token \
      python -m twine upload \
      --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

publish_on_test_pypi:
  stage: publish
  environment: pypi-test
  needs:
    - package_files
  when: manual
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "run twine for test pypi"
    # - |
    #   TWINE_PASSWORD=${PIPY_TOKEN} \
    #   TWINE_USERNAME=${PIPY_USERNAME} \
    # TODO: replace URL with a pipy URL
    #   python -m twine upload \
    #   --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

publish_on_pypi:
  stage: publish
  environment: pypi
  needs:
    - package_files
  when: manual
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
  script:
    - echo "run twine for pypi"
    # - |
    #   TWINE_PASSWORD=${PIPY_TOKEN} \
    #   TWINE_USERNAME=${PIPY_USERNAME} \
    # TODO: replace URL with a pipy URL
    #   python -m twine upload \
    #   --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

publish_to_readthedocs:
  stage: publish
  allow_failure: true
  environment: readthedocs
  needs:
    - package_docs
  when: manual
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
  script:
    - echo "scp docs/* ???"
    - exit 1
